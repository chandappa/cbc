package cbc

import (
	"errors"
	"sync"
	"time"

	gocb "github.com/couchbase/gocb/v2"
)

var (
	ErrClusterNotInialized = errors.New("cluster not initialized")
)

type CBC interface {
	CreateBucket(bucketSettings *gocb.BucketSettings) error
	OpenBucket(string) (*gocb.Bucket, error)
	CreateDoc(string, string, interface{}, *gocb.UpsertOptions) error
	GetDoc(string, string, interface{}) (gocb.Cas, error)
	DeleteDoc(string, string) error
	QueryDoc(query string) (*gocb.QueryResult, error)
	MutateInIncrementNumber(string, string, string) (int, error)
	Exists(string, string) (bool, error)
	ReplaceDoc(string, string, interface{}, *gocb.ReplaceOptions) error
	GetAndLockDoc(string, string, interface{}) (gocb.Cas, error)
	UnlockDoc(string, string, gocb.Cas) error
}

type cbc struct {
	mu                 sync.RWMutex
	cluster            *gocb.Cluster
	connectedBucketMap map[string]*gocb.Bucket
}

type Cfg interface {
	GetAddr() string
	GetUserName() string
	GetPassword() string
}

func New(cfg Cfg) (CBC, error) {

	var err error

	cbc := &cbc{
		connectedBucketMap: make(map[string]*gocb.Bucket),
	}

	err = cbc.connect(cfg)

	if err != nil {
		return nil, err
	}

	return cbc, nil
}

func (cbc *cbc) connect(cfg Cfg) error {

	opts := gocb.ClusterOptions{
		Authenticator: gocb.PasswordAuthenticator{
			Username: cfg.GetUserName(),
			Password: cfg.GetPassword(),
		},
	}

	cluster, err := gocb.Connect(cfg.GetAddr(), opts)

	if nil != err {
		return err
	}

	cbc.cluster = cluster

	return nil
}

/*
	gocb.BucketSettings{
		Name:                 "hello",
		FlushEnabled:         false,
		ReplicaIndexDisabled: true,
		RAMQuotaMB:           200,
		NumReplicas:          1,
		BucketType:           gocb.CouchbaseBucketType,
	},
*/

func (cbc *cbc) CreateBucket(bucketSettings *gocb.BucketSettings) error {
	bucketMgr := cbc.cluster.Buckets()

	err := bucketMgr.CreateBucket(
		gocb.CreateBucketSettings{
			BucketSettings:         *bucketSettings,
			ConflictResolutionType: gocb.ConflictResolutionTypeSequenceNumber,
		},
		nil)

	if err != nil {
		return err
	}

	// Adding some delay to give time to create the bucket
	// other wise index creation will fail
	time.Sleep(5 * time.Second)

	// Create a N1QL Primary Index (but ignore if one already exists).
	err = cbc.cluster.QueryIndexes().CreatePrimaryIndex(
		bucketSettings.Name, &gocb.CreatePrimaryQueryIndexOptions{
			IgnoreIfExists: true,
		})

	if err != nil {
		return err
	}

	return nil
}

func (cbc *cbc) OpenBucket(bktName string) (*gocb.Bucket, error) {

	if cbc.cluster == nil {
		return nil, ErrClusterNotInialized
	}

	bucket := cbc.cluster.Bucket(bktName)

	err := bucket.WaitUntilReady(5*time.Second, nil)

	if err != nil {
		return nil, err
	}

	cbc.connectedBucketMap[bktName] = bucket

	return bucket, nil
}

func (cbc *cbc) CreateDoc(bucketName string, key string, doc interface{}, upsertOptions *gocb.UpsertOptions) error {
	cbc.mu.Lock()
	defer cbc.mu.Unlock()
	var err error

	bucket, ok := cbc.connectedBucketMap[bucketName]

	if !ok {
		bucket, err = cbc.OpenBucket(bucketName)

		if err != nil {
			return err
		}
	}

	collection := bucket.DefaultCollection()
	_, err = collection.Upsert(key, &doc, upsertOptions)

	if err != nil {
		return err
	}

	return nil
}

func (cbc *cbc) DeleteDoc(bucketName string, key string) error {
	cbc.mu.Lock()
	defer cbc.mu.Unlock()
	var err error

	bucket, ok := cbc.connectedBucketMap[bucketName]

	if !ok {
		bucket, err = cbc.OpenBucket(bucketName)

		if err != nil {
			return err
		}
	}

	collection := bucket.DefaultCollection()

	_, err = collection.Remove(key, nil)

	if err != nil {
		return err
	}

	return nil
}

func (cbc *cbc) GetDoc(bucketName string, key string, doc interface{}) (gocb.Cas, error) {
	cbc.mu.Lock()
	defer cbc.mu.Unlock()
	var err error

	bucket, ok := cbc.connectedBucketMap[bucketName]

	if !ok {
		bucket, err = cbc.OpenBucket(bucketName)

		if err != nil {
			return 0, err
		}
	}

	collection := bucket.DefaultCollection()

	var result *gocb.GetResult
	result, err = collection.Get(key, nil)

	if err != nil {
		return 0, err
	}

	err = result.Content(doc)

	if err != nil {
		return 0, err
	}

	return result.Cas(), nil
}

func (cbc *cbc) QueryDoc(query string) (*gocb.QueryResult, error) {
	cbc.mu.Lock()
	defer cbc.mu.Unlock()

	results, err := cbc.cluster.Query(query, &gocb.QueryOptions{
		Adhoc: true,
	})

	if err != nil {
		return nil, err
	}

	return results, nil
}

func (cbc *cbc) MutateInIncrementNumber(bucketName string, key string, numberField string) (int, error) {
	cbc.mu.Lock()
	defer cbc.mu.Unlock()
	var err error

	bucket, ok := cbc.connectedBucketMap[bucketName]

	if !ok {
		bucket, err = cbc.OpenBucket(bucketName)

		if err != nil {
			return 0, err
		}
	}

	collection := bucket.DefaultCollection()

	mops := []gocb.MutateInSpec{
		gocb.IncrementSpec(numberField, 1, &gocb.CounterSpecOptions{}),
	}

	var result *gocb.MutateInResult

	result, err = collection.MutateIn(key, mops, &gocb.MutateInOptions{})

	if err != nil {
		return 0, err
	}

	var value int
	err = result.ContentAt(0, &value)

	if err != nil {
		return 0, err
	}

	return value, nil
}

func (cbc *cbc) Exists(bucketName string, key string) (bool, error) {
	cbc.mu.Lock()
	defer cbc.mu.Unlock()
	var err error

	bucket, ok := cbc.connectedBucketMap[bucketName]

	if !ok {
		bucket, err = cbc.OpenBucket(bucketName)

		if err != nil {
			return false, err
		}
	}

	var result *gocb.ExistsResult

	collection := bucket.DefaultCollection()

	result, err = collection.Exists(key, nil)
	if err != nil {
		return false, err
	}

	return result.Exists(), nil
}

func (cbc *cbc) ReplaceDoc(bucketName string, key string, doc interface{}, replaceOptions *gocb.ReplaceOptions) error {
	cbc.mu.Lock()
	defer cbc.mu.Unlock()
	var err error

	bucket, ok := cbc.connectedBucketMap[bucketName]

	if !ok {
		bucket, err = cbc.OpenBucket(bucketName)

		if err != nil {
			return err
		}
	}

	collection := bucket.DefaultCollection()

	// Attempt to replace the document using CAS
	_, err = collection.Replace(key, doc, replaceOptions)

	if err != nil {
		return err
	}

	return nil
}

func (cbc *cbc) GetAndLockDoc(bucketName string, key string, doc interface{}) (gocb.Cas, error) {
	cbc.mu.Lock()
	defer cbc.mu.Unlock()
	var err error

	bucket, ok := cbc.connectedBucketMap[bucketName]

	if !ok {
		bucket, err = cbc.OpenBucket(bucketName)

		if err != nil {
			return 0, err
		}
	}

	collection := bucket.DefaultCollection()

	var result *gocb.GetResult
	result, err = collection.GetAndLock(key, 10*time.Microsecond, nil)

	if err != nil {
		return 0, err
	}

	err = result.Content(doc)

	if err != nil {
		return 0, err
	}

	return result.Cas(), nil
}

func (cbc *cbc) UnlockDoc(bucketName string, key string, cas gocb.Cas) error {
	cbc.mu.Lock()
	defer cbc.mu.Unlock()
	var err error

	bucket, ok := cbc.connectedBucketMap[bucketName]

	if !ok {
		bucket, err = cbc.OpenBucket(bucketName)

		if err != nil {
			return err
		}
	}

	collection := bucket.DefaultCollection()

	err = collection.Unlock(key, cas, nil)

	if err != nil {
		return err
	}

	return nil
}
