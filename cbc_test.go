package cbc

import (
	gocb "github.com/couchbase/gocb/v2"
	"testing"
	"time"
)

type Reginfo struct {
	Type            string   `json:"type"`
	Version         string   `json:"@version"`
	State           string   `json:"@state,omitempty"`
	Counter         int      `json:"counter"`
	RegistrationIDs []string `json:"registration_ids"`
}

var (
	cluster CBC
)

type cfg struct {
}

func (cfg *cfg) GetAddr() string {
	return "couchbase://localhost"
}

func (cfg *cfg) GetUserName() string {
	return "Administrator"
}

func (cfg *cfg) GetPassword() string {
	return "Toothpaste"
}

func TestConnection(t *testing.T) {
	var err error
	cluster, err = New(&cfg{})

	if nil != err {
		t.Error(err)
	}
}

func TestCreateBucket(t *testing.T) {

	err := cluster.CreateBucket(
		&gocb.BucketSettings{
			Name:                 "reginfo_test",
			FlushEnabled:         false,
			ReplicaIndexDisabled: true,
			RAMQuotaMB:           200,
			NumReplicas:          0,
			BucketType:           gocb.CouchbaseBucketType,
		},
	)

	httpErr := err.(gocb.HTTPError)

	if nil != err && gocb.ErrBucketExists != httpErr.InnerError {
		t.Error(err)
	}
}

func TestOpenBucket(t *testing.T) {

	_, err := cluster.OpenBucket("reginfo_test")

	if nil != err {
		t.Error(err)
	}

}

func TestCreateDoc(t *testing.T) {

	doc := &Reginfo{
		Type:    "reginfo",
		Version: "1",
		State:   "Active",
		Counter: 0,
	}

	err := cluster.CreateDoc("reginfo_test", "001", doc, 180*time.Second)

	if nil != err {
		t.Error(err)
	}
}

func TestMutateInIncrementNumber(t *testing.T) {
	_, err := cluster.MutateInIncrementNumber("reginfo_test", "001", "counter")

	if nil != err {
		t.Error(err)
	}
}

func TestQueryDoc(t *testing.T) {

	query := "SELECT * FROM reginfo_test WHERE type=\"reginfo\" AND META(reginfo_test).id=\"001\""

	results, err := cluster.QueryDoc(query)

	if err != nil {
		t.Error(err)
	} else {
		var ri interface{}
		err = results.One(&ri)
		if nil != err {
			t.Error(err)
		}
	}
}

func TestGetDoc(t *testing.T) {

	ri := &Reginfo{}
	err := cluster.GetDoc("reginfo_test", "001", ri)

	if nil != err {
		t.Error(err)
	}
}

func TestDeleteDoc(t *testing.T) {

	err := cluster.DeleteDoc("reginfo_test", "001")

	if nil != err {
		t.Error(err)
	}
}
